package com.epam.beans;

public class Client {

	private Long id;
	private String name;
	private String greting;

	public Client(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGreting() {
		return greting;
	}

	public void setGreting(String greting) {
		this.greting = greting;
	}
	
	
}
