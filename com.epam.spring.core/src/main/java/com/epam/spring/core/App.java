package com.epam.spring.core;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.beans.Client;
import com.epam.beans.Event;
import com.epam.beans.EventType;
import com.epam.loggers.EventLogger;

public class App {
	
	@Autowired
	private Client client;
	private EventLogger logger;
	@Autowired
	private Event event;
	private Map<EventType, EventLogger> loggers;
	private static ConfigurableApplicationContext ap; 
	public App(  Map<EventType, EventLogger> loggers, EventLogger logger) {
		this.loggers = loggers;
		this.logger = logger;
	}
	
	public static void main(String[] args) {
		ap = new ClassPathXmlApplicationContext("spring.xml");
		App app = ap.getBean("app", App.class);
		app.logEvent(null,"hellx 1");
		app.logEvent(EventType.INFO,"hella 1");
		app.logEvent(EventType.ERROR,"helld 1");
		ap.close();

	}
	
	public void logEvent(EventType type, String msg){
		String message = msg.replaceAll(client.getId().toString(), client.getName());
		event = ap.getBean(Event.class);
		event.setMsg(message);
		EventLogger log = loggers.get(type);
		if(log==null){
			logger.logEvent(event);
		}else{
		log.logEvent(event);
		}
	}
	
	
}
