package com.epam.loggers.impl;

import com.epam.beans.Event;
import com.epam.loggers.EventLogger;

public class ConsoleEventLogger implements EventLogger{

	public void logEvent(Event event) {
		System.out.println(event);
	}
}
